﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }
        public Task<IEnumerable<T>> CreateAsync(T employee)
        {
            (Data as List<T>).Add(employee);
            return Task.FromResult(Data);
        }

        public Task<IEnumerable<T>> DeleteAsync(Guid id)
        {
            var listData = Data.ToList();
            listData.Remove(listData.FirstOrDefault(x => x.Id == id));
            Data = listData;
            return Task.FromResult(Data);
        }
        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> UpdateAsync(T employee)
        {
            var listData = Data.ToList();
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].Id == employee.Id)
                {
                    listData[i] = employee;
                    break;
                }
            }
            Data = listData;
            return Task.FromResult(Data);
        }
    }
}